# -*- coding: utf-8 -*-
{
    "name": "Report Aeroo Followup Bluepoint",
    "version": "8.0.1.0.4",
    "description": "Standard followup report which is used in the account_followup module",
    "author": "Jamotion GmbH",
    "website": "http://www.jamotion.ch",
    "category": "",
    "depends": [
        "report_aeroo_base",
        "account_followup",
        "account",
    ],
    "init_xml": [],
    "demo_xml": [],
    "data": ["views/ir_actions_report.xml",
    ],
    "installable": True,
    "active": False
}