# -*- coding: utf-8 -*-
from openerp.report import report_sxw
from openerp.report.report_sxw import rml_parse
from openerp.tools import mod10r
import logging

_logger = logging.getLogger(__name__)


class Parser(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        super(Parser, self).__init__(cr, uid, name, context)
        self.context = context
        self.localcontext.update({
            'filter_objects': self.filter_objects,
            'get_paid_amount': self.get_paid_amount,
            'get_amount_residual': self.get_amount_residual,
            'check_if_foreign_currency': self.check_if_foreign_currency,
            'get_foreign_currency': self.get_foreign_currency,
        })

    def filter_objects(self, objects):

        def filter_me(record):
            if self.get_amount_residual(record.unreconciled_aml_ids) <= 0:
                return False

            for line in record.unreconciled_aml_ids:
                if not line.blocked and (line.followup_date or (line.credit > 0 and not line.reconcile_ref)):
                    return True

            return False

        return objects.filtered(filter_me)

    def check_if_foreign_currency(self, lines):
        for line in lines:
            if line.currency_id:
                return True
        return False

    def get_foreign_currency(self, line):
        if not line.currency_id.id:
            return line.partner_id.property_product_pricelist.currency_id.name
        return line.currency_id.name

    def get_paid_amount(self, line, lines):
        paid_amount = 0.0
        for l in lines:
            if l.reconcile_ref and l.reconcile_ref == line.reconcile_ref and l.credit > 0:
                paid_amount += l.amount_currency and l.amount_currency * -1 or l.credit
        return paid_amount

    def get_amount_residual(self, lines):
        amount_residual = 0.0

        for line in lines:
            if not line.blocked:
                if line.followup_date:
                    amount_residual += line.amount_currency and line.amount_currency or line.debit
                elif line.credit > 0:
                    amount_residual += line.amount_currency and line.amount_currency or line.credit * -1
        return amount_residual
