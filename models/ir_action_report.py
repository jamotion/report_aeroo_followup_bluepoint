# -*- coding: utf-8 -*-
#
#    Jamotion GmbH, Your Odoo implementation partner
#    Copyright (C) 2013-2015 Jamotion GmbH.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#    Created by renzo.meister on 13.06.2015.
#
from openerp import models, api
import logging
import openerp
import base64
import os

_logger = logging.getLogger(__name__)


class IrActionsReport(models.Model):

    _inherit = "ir.actions.report.xml"

    # load the aeroo template to database and set the out format type
    @api.multi
    def upload_binary_file_to_db(self, file_path):
        addons_path = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))
        relative_path = os.path.relpath(file_path, addons_path)

        # read binary file
        with open(file_path, mode='rb') as file:
            file_content = file.read()

        vals = {
            'report_sxw_content_data': base64.b64encode(file_content),
            'report_rml': relative_path,
        }

        self.write(vals)

    @api.multi
    def _upload_report(self, file_name):
        file_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'report', file_name)
        self.upload_binary_file_to_db(file_path)

    # load aeroo template to database
    @api.multi
    def load_temp_to_db(self, file_name):
        self._upload_report(file_name)

